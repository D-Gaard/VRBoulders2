using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rayRightHand : MonoBehaviour
{
  public LineRenderer line;
  public float lineWidth = 0.1f;
  public float lineMaxLength = 1f;
  public string tag_;
  public bool toggled = false;
  public Material onHitMaterial;
  public float magnitude = 10;

  private float HandRight = OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger);

  private GameObject target;

  // Start is called before the first frame update
  void Start()
  {
    Vector3[] startLinePositions = new Vector3[2] { Vector3.zero, Vector3.zero };
    line.SetPositions(startLinePositions);
    line.enabled = false;
  }

  // Update is called once per frame
  void Update()
  {
    HandRight = OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger);

    if (HandRight > 0.9)
    {
      toggled = true;
      line.enabled = true;
    }
    else
    {
      line.enabled = false;
      toggled = false;
    }

    if (toggled){
      lineDraw(transform.position, transform.forward, lineMaxLength);
    }
  }
  private void lineDraw(Vector3 targetPosition,Vector3 direction,float length){
    RaycastHit hit;
    Ray lineOut = new Ray(targetPosition, direction);
    Vector3 endPosition = targetPosition + (length * direction);

    if(Physics.Raycast(lineOut,out hit)){
      endPosition = hit.point;
      target = hit.collider.gameObject;

      if(target.tag == tag_){
        var force = targetPosition - endPosition;

        force.Normalize();
        target.GetComponent<Rigidbody>().AddForce(force * magnitude);
        target.GetComponent<MeshRenderer>().material = onHitMaterial;

      }

      line.SetPosition(0, targetPosition);
      line.SetPosition(1, endPosition);
    }
  }
}
