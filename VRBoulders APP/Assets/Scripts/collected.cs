using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collected : MonoBehaviour
{
    new public int Pickups = 0;
    new public GameObject pickup1;
    new public GameObject pickup2;
    new public GameObject pickup3;
    new public GameObject pickup4;

    void OnTriggerEnter(Collider pickup){
        if(pickup.gameObject.tag == "Opsamlingsting" && Pickups == 0){
            Pickups = 1;
            pickup2.SetActive(true);
        }
        else if(pickup.gameObject.tag == "Opsamlingsting" && Pickups == 1){
            Pickups = 2;
            pickup3.SetActive(true);
        }
        else if(pickup.gameObject.tag == "Opsamlingsting" && Pickups == 2){
            Pickups = 3;
            pickup4.SetActive(true);
        }
    } 
}
